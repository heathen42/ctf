# CTF Related stuff

## Table of Contents

The challs are linked in the table below.

| Description | Event/Game | Solution |
|:------------:|:----:|:---------:|
|**Smash The Stack**|---|---|
|Blackbox|[Level 1](./work/smashthestack/blackbox/level1.md)|An exe with a "login" functionality with the password in plain text in the memory.|
|Blackbox|[Level 2](./work/smashthestack/blackbox/level2.md)|An exe with `setuid` for root. A **return-to-libc** vuln.|
