# Writeup - level 1

1. there is an elf called "level2" that simply asks for a password

2. opening it with gdb
    - the password we type is saved at eax
    - it compares input with "PassFor2"
        ---> that's the fucking password -.-, I was trying to "su" into the level2 user, but I had to ssh into it
    - if it is equal, it spawns "/bin/sh" with the same user
    - set {char [16]}0x080485ff="la /home/level2"
        -> at least I learnt this lol
        -> and this "set $eip=*(main+94)" to jump to an offset from main function

3. I could just have used `strings`
