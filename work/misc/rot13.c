#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main (int argc, unsigned char * argv[]) {
    if (argc != 2) {
        printf("Usage: %s 'content'\n", argv[0]);
        exit(1);
    }

    for (int i = 0; i < strlen(argv[1]); i++) {
        if (argv[1][i] >= 97 && argv[1][i] <= 122) {
            argv[1][i] += 13;

            if (argv[1][i] > 122)
                argv[1][i] = 96 + (argv[1][i] - 122);
        } else if (argv[1][i] >= 65 && argv[1][i] <= 90) {
            argv[1][i] += 13;

            if (argv[1][i] > 90)
                argv[1][i] = 64 + (argv[1][i] - 90);
        }

        printf("%c - %d\n", argv[1][i], argv[1][i]);
    }

    printf("\n");

    return 0;
}


